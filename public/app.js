var app = new Vue({
  el: '#app',
  data: {
    waiting: true,
    lefttile: 6,
    righttile: 0,

    map: {
      cols: 32,
      rows: 32,
      tsize: 64,
      ntiles: 64,
      tiles: [
        1, 2, 3, 4
      ]
    }
  },

  methods: {
    getTile: function (col, row) {
      return app.map.tiles[row * app.map.cols + col];
    },
    setTile: function (col, row, value) {
      if (col >= app.map.cols) return;
      if (col < 0) return;
      if (row >= app.map.rows) return;
      if (row < 0) return;
      app.waiting = true;
      WS.send(col + " " + row + " " + value)
      //app.map.tiles[row * app.map.cols + col] = value;
    },
    applyTile: function (col, row, value) {
      app.map.tiles[row * app.map.cols + col] = value;
    },
    myping: function () {
      app.waiting = true;
      WS.send("63 63 0")
    }

  },

  computed: {
  },
});

Game.load = function () {
  return [
    Loader.loadImage('background', './tiles.png'),
    // Loader.loadImage('characters', './characters.png'),
  ];
};

Game.init = function () {
  this.tileBackground = Loader.getImage('background');
  // this.tileChars = Loader.getImage('characters');
};

function draw_tile(ctx, atlas, x, y, tile, tilesize) {
  if (tile < 0) return;
  if (tile > 64) return;
  sy = Math.trunc(tile / 8) * tilesize;
  sx = (tile % 8) * tilesize;
  ctx.drawImage(atlas, sx, sy, tilesize, tilesize, x * tilesize, y * tilesize, tilesize, tilesize);
}

Game.render = function () {
  width = 32
  atlaswidth = 8
  for (var x = 0; x < atlaswidth; x++)
    for (var y = 0; y < atlaswidth; y++)
      draw_tile(this.lefttool, this.tileBackground, (x + y * atlaswidth) % width, Math.trunc((x + y * atlaswidth) / width), x + y * atlaswidth, app.map.tsize);
  draw_tile(this.lefttool, this.tileBackground, app.lefttile % width, Math.trunc(app.lefttile / width), 63, app.map.tsize);

  for (var x = 0; x < atlaswidth; x++)
    for (var y = 0; y < 2; y++)
      draw_tile(this.righttool, this.tileBackground, x + y * atlaswidth, 0, x + y * atlaswidth, app.map.tsize);
  draw_tile(this.righttool, this.tileBackground, app.righttile, 0, 63, app.map.tsize);


  for (var c = 0; c < app.map.cols; c++) {
    for (var r = 0; r < app.map.rows; r++) {
      draw_tile(this.ctx, this.tileBackground, c, r, app.getTile(c, r), app.map.tsize)
    }
  }
};

function windowToCanvas(canvas, x, y) {
  var bbox = canvas.getBoundingClientRect();
  return {
    x: x - bbox.left * (canvas.width / bbox.width),
    y: y - bbox.top * (canvas.height / bbox.height)
  };
}

function canvasmouseup(e) {
};

function canvasmousedown(e) {
  canvas = Game.ctx.canvas
  if (e.button == 0)
    tile = app.lefttile
  else
    tile = app.righttile
  var loc = windowToCanvas(canvas, e.clientX, e.clientY);
  app.setTile(Math.trunc(loc.x / 64), Math.trunc(loc.y / 64), tile)
};

function canvasmousemove(e) {
  if (e.buttons == 0) return;
  canvas = Game.ctx.canvas
  if (e.buttons == 1)
    tile = app.lefttile
  else
    tile = app.righttile
  var loc = windowToCanvas(canvas, e.clientX, e.clientY);
  app.setTile(Math.trunc(loc.x / 64), Math.trunc(loc.y / 64), tile)
};

function selectleft(e) {
  canvas = Game.lefttool.canvas
  var loc = windowToCanvas(canvas, e.clientX, e.clientY);
  width = 32
  app.lefttile = Math.trunc(loc.x / 64) + Math.trunc(loc.y / 64) * width
};

function selectright(e) {
  canvas = Game.righttool.canvas
  var loc = windowToCanvas(canvas, e.clientX, e.clientY);
  app.righttile = Math.trunc(loc.x / 64)
};


function wserror(event) {
  console.log(event.data);
  WS = new WebSocket('ws://185.255.133.239:3000/ws');
  WS.onerror = wserror
  WS.onmessage = wsmessage
};

function wsmessage(event) {
  all = JSON.parse(event.data);
  if (all.full)
    app.map = all.map;
  else {
    app.applyTile(all.x, all.y, all.v)
  }
  app.waiting = false;
};
WS = new WebSocket('ws://185.255.133.239:3000/ws');
WS.onmessage = wsmessage
WS.onerror = wserror

