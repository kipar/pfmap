//
// Asset loader
//

var Loader = {
    images: {}
};

Loader.loadImage = function (key, src) {
    var img = new Image();

    var d = new Promise(function (resolve, reject) {
        img.onload = function () {
            this.images[key] = img;
            resolve(img);
        }.bind(this);

        img.onerror = function () {
            reject('Could not load image: ' + src);
        };
    }.bind(this));

    img.src = src;
    return d;
};

Loader.getImage = function (key) {
    return (key in this.images) ? this.images[key] : null;
};

//
// Game object
//

var Game = {};

Game.run = function (context) {
    this.ctx = context.map;
    this.lefttool = context.lefttool;
    this.righttool = context.righttool;
    this._previousElapsed = 0;

    var p = this.load();
    Promise.all(p).then(function (loaded) {
        this.init();
        window.requestAnimationFrame(this.tick);
    }.bind(this));
};

Game.tick = function (elapsed) {
    window.requestAnimationFrame(this.tick);

    // clear previous frame
    //this.ctx.clearRect(0, 0, 512, 512);
    this.lefttool.clearRect(0, 0, 4096, 128);
    this.righttool.clearRect(0, 0, 2048, 64);

    // compute delta time in seconds -- also cap it
    var delta = (elapsed - this._previousElapsed) / 1000.0;
    delta = Math.min(delta, 0.25); // maximum delta of 250 ms
    this._previousElapsed = elapsed;

    this.update(delta);
    this.render();
}.bind(Game);

// override these methods to create the demo
Game.init = function () { };
Game.update = function (delta) { };
Game.render = function () { };

//
// start up function
//

window.onload = function () {
    var context = { map: document.getElementById('demo').getContext('2d'), lefttool: document.getElementById('leftbutton').getContext('2d'), righttool: document.getElementById('rightbutton').getContext('2d') };
    Game.run(context);
};
