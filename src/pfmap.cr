require "kemal"
require "kemal-session"
require "random/secure"

NCOLS    = 64
NROWS    = 64
TILESIZE = 64
NTILES   = 8*8

class Game
  getter map = Array(Int32).new(NCOLS*NROWS, 0)

  def set_tile(message)
    x, y, v = message.split(' ').map(&.to_i)
    return unless (0...NCOLS).includes?(x)
    return unless (0...NROWS).includes?(y)
    return unless (0...NTILES).includes?(v)
    map[x + y*NCOLS] = v
    {full: false, x: x, y: y, v: v}.to_json
  end

  def for_json
    {
      full: true,
      map:  {
        cols:   NCOLS,
        rows:   NROWS,
        tsize:  TILESIZE,
        ntiles: NTILES,
        tiles:  map,

      },
    }.to_json
  end
end

GAME    = Game.new
SOCKETS = [] of HTTP::WebSocket

def check_sockets
  SOCKETS.each do |s|
  end
end

error 404 do
  text = "Страница не найдена"
  render "src/views/error.ecr", "src/views/layout.ecr"
end
error 403 do
  text = "Доступ запрещен"
  render "src/views/error.ecr", "src/views/layout.ecr"
end
get "/" do |env|
  env.response.status_code = 403
end

get "/reset" do |env|
  SOCKETS.clear
  text = "сокеты сброшены"
  render "src/views/error.ecr", "src/views/layout.ecr"
end

get "/log" do |env|
  text = File.read "/var/www/pfmap/nohup.out"
  render "src/views/error.ecr", "src/views/layout.ecr"
end

ws "/ws" do |socket|
  puts "Opening socket: #{SOCKETS.size}"
  socket.on_message do |message|
    begin
      puts "socket: #{message}"
      msg = GAME.set_tile(message)
      if msg
        failedsockets = [] of HTTP::WebSocket
        SOCKETS.each do |sock|
          begin
            sock.send(msg)
          rescue ex
            puts "Failed socket: #{ex.inspect_with_backtrace}"
            failedsockets << sock
          end
        end
        failedsockets.each do |sock|
          SOCKETS.delete sock
        end
        puts "resent to #{SOCKETS.size}"
      end
    rescue ex
      SOCKETS.delete socket
      puts "Error on message (#{SOCKETS.size}): #{ex.inspect_with_backtrace}"
    end
  end
  socket.on_close do
    SOCKETS.delete socket
    puts "Closing socket: #{SOCKETS.size}"
  end
  socket.send GAME.for_json
  SOCKETS << socket
  puts "Map sent: #{SOCKETS.size}"
end

Kemal.run do |config|
  server = config.server.not_nil!
  server.bind_tcp "0.0.0.0", 3000, reuse_port: true
end
